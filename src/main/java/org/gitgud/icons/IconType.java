package org.gitgud.icons;

/**
 * All icon types.
 */
public enum IconType {

    /**
     *
     */
    ICON_LIGHT, ICON_DARK, ICON_COLORED,

}
